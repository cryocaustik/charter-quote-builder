var app = new Vue({
    el: "#vueApp",
    data: {
        client: {
            clientName: '',
            clientOrg: '',
        }, 
        vehTypes: ["Motor Coach", "Party Bus", "Limo"],
        vehType: '',
        hours: 2,
        vehHrRates: {
            'Motor Coach': 140.00,
            'Party Bus': 120.00,
            'Limo': 90.00
        },
        feeMarkup: 350,
        feeDiscountPerc: 11.5,
        affFeeHr: 0,
        affFeeTotal: 0,
        affFeeTotalDiscounted: 0,
        resDt: '',
        affFeeTotalStr: '',
    },
    methods: {
        affiliateHourly: function(){
            this.affFeeHr = this.vehHrRates[this.vehType];
            this.affiliateTotal();
        },
        affiliateTotal: function (){
            this.affFeeTotal =  parseFloat(this.feeMarkup) + (this.affFeeHr * this.hours);
            this.affFeeTotalDiscounted = this.affFeeTotal - (this.affFeeTotal * (this.feeDiscountPerc/100));
            this.clientCostStr();
        },
        clientCostStr: function(){
            this.affFeeTotalStr = '$' + this.affFeeTotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    }
});
